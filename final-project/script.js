'use strict';

// accordion variables
const accordionBtn = document.querySelectorAll('[data-accordion-btn]');
const accordion = document.querySelectorAll('[data-accordion]');

for (let i = 0; i < accordionBtn.length; i++) {
  accordionBtn[i].addEventListener('click', function () {
    const clickedBtn = this.nextElementSibling.classList.contains('active');
    for (let i = 0; i < accordion.length; i++) {
      if (clickedBtn) break;
      if (accordion[i].classList.contains('active')) {
        accordion[i].classList.remove('active');
        accordionBtn[i].classList.remove('active');
      }
    }
    this.nextElementSibling.classList.toggle('active');
    this.classList.toggle('active');
  });
}

// mobile menu variables
const mobileMenuOpenBtn = document.querySelectorAll('[data-mobile-menu-open-btn]');
const mobileMenu = document.querySelectorAll('[data-mobile-menu]');
const mobileMenuCloseBtn = document.querySelectorAll('.menu-close-btn');
const overlay = document.querySelector('[data-overlay]');

for (let i = 0; i < mobileMenuOpenBtn.length; i++) {

  // mobile menu function
  const mobileMenuCloseFunc = function () {
    mobileMenu[i].classList.remove('active');
    overlay.classList.remove('active');
  }

  mobileMenuOpenBtn[i].addEventListener('click', function () {
    mobileMenu[i].classList.add('active');
    overlay.classList.add('active');
  });

  
  overlay.addEventListener('click', mobileMenuCloseFunc);
  mobileMenuCloseBtn[i].addEventListener('click', mobileMenuCloseFunc);

}

// Email Validation
function validateEmail(email)
{
    var regex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    return regex.test(email);
}

// Form Submission
function submitForm()
{
    const name = document.getElementById("name-input").value;
    const email = document.getElementById("email-input").value;
    const message = document.getElementById("message-input").value;

    if (name.length > 3)
    {
        if (validateEmail(email) == true)
        {
            if (message.length > 10)
            {
                document.getElementById("formMessage").innerText = "Form submitted successfully!";
                document.getElementById("formMessage").style.color = "green";
                document.getElementById("contactForm").reset();
            }
            else
            {
                document.getElementById("formMessage").innerText = "Message too short!";
                document.getElementById("formMessage").style.color = "red";
            }
        }
        else
        {
            document.getElementById("formMessage").innerText = "Please enter a valid email address!";
            document.getElementById("formMessage").style.color = "red";
        }
    }
    else
    {
        document.getElementById("formMessage").innerText = "Name too short!";
        document.getElementById("formMessage").style.color = "red";
    }
}

// Simulated User Database
const userDatabase = [
    { username: 'admin', password: 'admin' },
    { username: 'cashier', password: 'iluvstationaryshop' },
];

// Login Function
function login()
{
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    var errorMessage = document.getElementById('error-message');

    // Check if the user exists in the simulated database
    const user = userDatabase.find(user => user.username === username && user.password === password);

    if (user)
    {
        // Successful login, redirect or perform additional actions
        alert("Login successful!");
        window.location.href = 'home.html';
    }
    else
    {
        // Display error message
        errorMessage.innerHTML = 'Invalid username or password';
        errorMessage.style.color = 'red';
    }
}

// Sign Up Function
function signup() {
    var username = document.getElementById('username').value;
    var fullName = document.getElementById('fullname').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var errorMessage = document.getElementById('error-message');

    // Dummy validation for this example
    if (username && fullName && email && password) {
      // Check if the user already exists in the database
      var userExists = userDatabase.some(
        function (user)
        {
            return user.username === username;
        }
        );

      if (userExists)
      {
        errorMessage.innerHTML = 'User already exists. Please choose a different username.';
        errorMessage.style.color = 'red';
      }
      else if (validateEmail(email) == false)
      {
        errorMessage.innerHTML = 'Please enter a valid email address.';
        errorMessage.style.color = 'red';
      }
	  else if (password.length < 8)
	  {
		errorMessage.innerHTML = 'Password must be at least 8 characters long.';
		errorMessage.style.color = 'red';
	  }
      else
      {
        // Add the new user to the database
        userDatabase.push({ username: fullName, password: password });
        alert("Sign up successful! Redirecting to the home page.");

        // Redirect to the home page or perform other actions as needed
        window.location.href = 'home.html';
      }
    }
    else
    {
        errorMessage.innerHTML = 'Please fill in all the fields';
        errorMessage.style.color = 'red';
    }
}

// Reset Password Function
function resetPassword() {
    var email = document.getElementById('emails').value;
    var errorMessage = document.getElementById('error-messages');

    if (validateEmail(email) == false) {
		errorMessage.innerHTML = 'Please enter a valid email address.';
        errorMessage.style.color = 'red';
    }
    else
    {
        alert("The reset password link has been sent to your email! Redirecting to the home page.");
        window.location.href = 'home.html';
    }
}

document.getElementById("modifyDate").innerHTML = new Date(document.lastModified);
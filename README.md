# sun-web1201

Codebase for WEB1201 subject.

## Assignment To-Do List

- [x] Contact Form
- [x] Sign Up & Log In Function
- [x] No issue scaling or layout on different viewport sizes
- [x] May create database to store users' information (Hardcoded using Javascript)

## Wilson To-Do List
- [x] Finish Sign Up function and adding it to the database
- [x] Finish unifying all images width and height for better display
- [x] Mobile view home menu currently not working
- [x] Add more content to the home page & offer page
- [x] Fix Mobile View (Sign Up and Login Button)
- [x] Fix email and tel number in footer
- [x] Copy header from contact.html to other html files
- [x] Forget password page
- [x] Date, creation (Website)
- [x] Add forget password page to report
- [x] Update report for footer
- [ ] 5 mins video

## Reference
- https://www.pacificwise.com.my/